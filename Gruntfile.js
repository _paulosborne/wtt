module.exports = function (grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    requirejs: {
      compile: {
        options: {
          baseUrl: "js/",
          mainConfigFile: 'js/build.js',
          paths: {
              tpl: 'lib/tpl/tpl'
          },
          name: "bootstrap",
          out: "js/dist/bundle.js",
          optimize: 'none'
        }
      }
    },
    /**
     * Karma
     */
    karma: {
      unit: {
        configFile: 'karma.conf.js',
        singleRun: true
      }
    },
    /**
     * Clean
     */
    clean: {
      build: {
        src: ["js/dist"]
      }
    },
    /**
     * Watch
     */
    watch: {
      scripts: {
        files: ['js/**/*.js'],
        tasks: ['default'],
        options: {
          debounceDelay: 250
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-karma');

  grunt.registerTask('default',['requirejs','karma']);
}
