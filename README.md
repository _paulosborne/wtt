
### Features

* Can create sheep, cows, birds or goats.
* Animals can be branded.

### Instructions
* When breeding, the animal type will be used to determine which animals will breed.
* Clicking add without a name will pick one at random.

### Breeding Rules

* There is a 50% chance of success.
* Only animals of the same type can breed.
* Branded animals can't breed.
* Animals cannot breed with their descendents.

Example.

```
Sheep A (male) breeds with Sheep B and produces Sheep C.
Sheep C breeds with Sheep E and produces Sheep G.
Sheep A cannot breed with sheep C or G as they're are his descendents.
```

### Further Work/Improvements

* Disallow males to to mate with their mothers.
* Refactor the breeding algorithm a little.
* Write the frontend in Refactor
* Add a canvas element to visualise the sheep, and have them moo-ve around.
* Add more tests.


### Original Instructions
```
The Wren Field JavaScript Test
------------------------------
The fields are green and Wren are venturing out into the world as a shepherd.


The Task
--------
Your task is to extend the provided code with functionality and logic to add sheep to the field and allow them to breed.

The rows in the HTML table illustrate sheep that are currently within the field.

The specification of what is required is as follows:

1) You must be able to name and add a sheep into the field, each as a male or female
2) You must be able to brand a sheep at random (paint it green) that is currently living in the field
3) You must encourage two random sheep to breed and if successful, spawn a new sheep into the field
4) Sheep that are born from existing sheep in the field should have their parents denoted in the table

Logic that must be factored in:

a) The probability of a female sheep giving birth after mating is 50%
b) Branded sheep must be highlighted in green
c) Branded sheep are not able to breed

Use common sense to define your rules about which sheep can breed with each other, and include them when you return your submission.


What's Included?
----------------
Included is some skeleton code for you to work with that comprises of a simple HTML page, Twitter Bootstrap and a barebones JavaScript frame to work upon.
The JavaScript comes pre-bootstrapped using RequireJS (http://www.requirejs.org/) as a module loader and includes a simple embedded JS template library that is already being used within the page.
The file named indexcontroller.js contains an example that adds a hard-coded sheep that you should replace. Try out the example by clicking on the 'Add Sheep (F)' button.


Please Note
-----------
RequireJS expects to be able to make XHR requests to fetch data.
Some browsers have restrictions regarding this, so we advise that you serve the application from a HTTP server or run it using FireFox.
See https://github.com/requirejs/text#configuration about running for more information.


Further Work
------------
Should you have any spare time, briefly describe how you would extend the application to take into account other animals that may co-exist within the field, such as cows or goats.


Finished?
---------
Please bundle up your submission and return it to: candidate.test@wrenkitchens.com
Remember to include any assumptions you have made whilst doing the task.


Good luck!
```
