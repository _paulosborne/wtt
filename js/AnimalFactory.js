define(function (require) {
  var Cow = require('./animals/Cow');
  var Sheep = require('./animals/Sheep');
  var Bird = require('./animals/Bird');

  var randomNames = {
    'female': ['fluffykins','ursula','lady','daisy'],
    'male': ['james','trigger','grumpy','leonard','dexter']
  }

  function AnimalFactory () {
    var _id = 0;
    var animalType = Sheep;

    /**
     * Return a random name for provided gender
     */
    function pickRandomName (sex) {
      return randomNames[sex][Math.floor(Math.random() * randomNames[sex].length)]
    }

    function pickRandomGender () {
      var genders = ['male','female'];

      return genders[Math.floor(Math.random() * genders.length)];
    }

    /**
     * Creates a new animal, default is sheep
     */
    function create (options) {
      var child;

      if (!options.type) {
        throw new Error('please specify the type of animal to create');
      }

      if (!options.hasOwnProperty('sex')) {
        options.sex = pickRandomGender();
      }

      if (!options.hasOwnProperty('name') || options.name == "") {
        options.name = pickRandomName(options.sex);
      }

      return new animalType(options);
    }

    return {
      create: create
    }
  }

  return AnimalFactory
})
