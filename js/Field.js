define("Field", function (Field) {
  var template = require('tpl!./templates/field.tpl');
  var Animal = require('./animals/Animal');

  var hasOwn = Object.prototype.hasOwnProperty;
  function has (obj, key) { return hasOwn.call(obj, key) }

  function Field (o) {
    var _animals = [];
    var _id = 0;
    var _maxAnimals = 100;

    /**
     * Add an animal to the field
     *
     * @param {animals}
     * @param {number} n - number of animals to add
     */
    function add (attributes) {
      if (_animals.length >= _maxAnimals) return false;

      var animal =  Object.create(Animal);
      attributes.id = ++_id

      animal.init(attributes);
      _animals.push(animal)

      console.log(animal)

      return _animals.map(function (x) {
        return x.id;
      });
    }

    /**
     * Attempt to find a love match for one lucky contestant.
     *
     * @param {object} all - the animals to look for a match
     * @param {object} subject - someone looking for love
     * @param {array} matches - animals that meet our requirements
     * @param {number} total - the max number of matches to return
     * @return {array}
     */
    function findLove (all, subject, matches, total) {
      var i, match, result = true;

      if (all.length === 0) {
        return matches;
      }

      i = all.randomIndex();
      match = all.splice(i, 1)[0];


      if (subject.sex === match.sex || subject.type !== match.type) {
        result = false
      } else {

        // prevent breeding with parents
        if (has(subject, 'parents')) {
          if (subject.parents.indexOf(match.id) !== -1) {
            result = false
          }
        }

        // prevent breeding with children
        if (has(match, 'parents')) {
          if (match.parents.indexOf(subject.id) !== -1) {
            result = false;
          }
        }

        // prevent breeding with siblings that have the same mother
        if (has(subject,'parents') && has(match,'parents')) {
          result = subject.parents[1] !== match.parents[1];
        }
      }

      if (result) {
        matches.push(match.id);
      }

      findLove(all, subject, matches);
    }


    function find (params) {
      var keys = Object.keys(params);
      return _animals.filter(function (animal) {
        var result = keys.filter(function (x) {
          return has(animal, x) && animal[x] === params[x];
        })

        return result.length === keys.length;
      })
    }

    function findMatchingPair () {
      var unbranded = find({ branded: false });
      var matches = [];

      console.log(unbranded[0])
      // findLove(unbranded, unbranded[0], matches, 1)

    }

    /**
     * Encourage two random animals to breed
     * @param {animalType}
     * @return {object|boolean}
     */
    function breed () {
      var unbranded = find({ branded: false });
      var matches = [];

      unbranded.find(function (x) {
        console.log(arguments)
      }, matches)

      // // get all unbranded animals
      // var random = unbranded.splice(Math.floor(Math.random() * unbranded.length),1);
      // var matches = [];
      //
      // findLove(unbranded, random, matches)
      //
      // console.log(matches);



      // recurse through the undbranded animals and try to find a match





      // if (!animalType) {
      //   throw new Error('missing animal type');
      // }

      // We only want animals of the provided animal type.
      // res = _animals.filter(function (x) {
      //   return x.type === animalType;
      // }).filter(isNotBranded);

      // if (res.length === 0) {
      //   throw new Error('there are no unbranded sheep');
      // }

      // filter parents, children & siblings
      // res = res.filter(filterParents)
      //          .filter(filterChildren)
      //          .filter(filterSiblings);

      // if (res.length === 0) {
      //   throw new Error('theres nobody to mate with');
      // }

      // group results by gender
      // res = res.reduce(reduceByGender, {})

      // we need both males and females to mate.
      // if (!res.hasOwnProperty(_M) || !res.hasOwnProperty(_F)) {
      //   throw new Error('we need both males and females to mate');
      // }

      // randomly select a lucky lady.
      // female = randArrayVal(res[_F]);


      // randomly select one of her matches.
      // male  = getAnimalById(randArrayVal(female.matches));

      // 50% chance of having a baby - fingers crossed!
      // if (!magicHappens()) return;

      // var child = _factory.createAnimal({
      //   type: animalType,
      //   parents: [male.id, female.id]
      // });
      //
      // female.children.push(child.id);
      // male.children.push(child.id);
      //
      // addAnimal(child);

      return child;
    }

    /**
     * Ren ders the Field
     * @return {string}
     */
    function render () {
      return template({'animals': _animals });
    }

    // Public Interface
    return {
      add: add,
      find: find,
      findLove: findLove,
      findMatchingPair: findMatchingPair,
      breed: breed
      // getAll: getAll
      // breed: breed,
      // brand: brand,
      // countAnimals: countAnimals,
      // isNotBranded: isNotBranded,

      // get: get,
      // getAll: getAll,
      // render: render
    }
  }

  return Field;

});
