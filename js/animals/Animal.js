define(['tpl!../templates/animal.tpl'], function (template) {
  var id = 0;

  var Animal = {
    init: function (options) {
      this.id = options.id;
      this.type = options.type;
      this.branded = options.branded || false;
      this.parents = options.parents || [];
      this.sex = options.sex || 'male';
      this.name = options.name;
    },
    brand: function () {
      this.branded = true;
    },
    render: function () {
      return template({
        'id': this.id,
        'type': this.type,
        'name': this.name,
        'gender': this.sex,
        'branded': this.branded,
        'parents': this.parents.join(' + ')
      });
    }
  }

  // function Animal (opts) {
  //   if (!opts) opts = {}
  //   this.type = opts.type;
  //   this.parents = opts.parents || [];
  //   this.branded = opts.branded || false;
  //   this.sex = opts.sex || ['male','female'].random();
  //   this.name = opts.name;
  //
  //   function randomGender () {
  //     var genders = ['male','female'].random();
  //   }
  // }
  //
  // Animal.prototype.brand = function () {
  //   this.branded = true;
  //   return this;
  // }
  //
  // Animal.prototype.render = function () {
    // return template({
    //     'id': this.id,
    //     'type': this.type,
    //     'name': this.name,
    //     'gender': this.sex,
    //     'branded': this.branded,
    //     'parents': this.parents.join(' + ')
    // });
  // }

  return Animal;
});
