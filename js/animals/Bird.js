define(['./Animal'], function (Animal) {
  function Bird (options) {
    Animal.call(this, options);
    this.legs = 2;
    this.phrases = ['ca-caw!','tweet!'];
    this.canFly = true;
  }

  Bird.prototype = Object.create(Animal.prototype);

 return Bird;
});
