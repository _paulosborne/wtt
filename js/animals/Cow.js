define(['./Animal'], function (Animal) {
  function Cow (options) {
    Animal.call(this, options);
    this.legs = 4;
    this.phrases = ['moo','moooOOOooo'];
  }

  Cow.prototype = Object.create(Animal.prototype);

  return Cow;
});
