define(['./Animal'], function (Animal) {
  
  function Goat (options) {
    Animal.call(this, options);
    this.legs = 4;
    this.phrases = ['moo','moooOOOooo'];
  }

  Goat.prototype = Object.create(Animal.prototype);

  return Goat;
});
