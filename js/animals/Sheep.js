define(['./Animal'], function (Animal) {
  
  function Sheep (options) {
    Animal.call(this, options);
    this.legs = 4;
    this.phrases = ['baa','baa-ram-you'];
  }

  Sheep.prototype = Object.create(Animal.prototype);

  return Sheep;
})
