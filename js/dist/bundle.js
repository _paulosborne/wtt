/**
 * Adapted from the official plugin text.js
 *
 * Uses UnderscoreJS micro-templates : http://documentcloud.github.com/underscore/#template
 * @author Julien Cabanès <julien@zeeagency.com>
 * @version 0.2
 * 
 * @license RequireJS text 0.24.0 Copyright (c) 2010-2011, The Dojo Foundation All Rights Reserved.
 * Available via the MIT or new BSD license.
 * see: http://github.com/jrburke/requirejs for details
 */
/*jslint regexp: false, nomen: false, plusplus: false, strict: false */
/*global require: false, XMLHttpRequest: false, ActiveXObject: false,
  define: false, window: false, process: false, Packages: false,
  java: false */

(function () {
	var progIds = ['Msxml2.XMLHTTP', 'Microsoft.XMLHTTP', 'Msxml2.XMLHTTP.4.0'],
	
		xmlRegExp = /^\s*<\?xml(\s)+version=[\'\"](\d)*.(\d)*[\'\"](\s)*\?>/im,
		
		bodyRegExp = /<body[^>]*>\s*([\s\S]+)\s*<\/body>/im,
		
		buildMap = [],
		
		templateSettings = {
			evaluate	: /<%([\s\S]+?)%>/g,
			interpolate : /<%=([\s\S]+?)%>/g
		},

		/**
		 * JavaScript micro-templating, similar to John Resig's implementation.
		 * Underscore templating handles arbitrary delimiters, preserves whitespace,
		 * and correctly escapes quotes within interpolated code.
		 */
		template = function(str, data) {
			var c  = templateSettings;
			var tmpl = 'var __p=[],print=function(){__p.push.apply(__p,arguments);};' +
				'with(obj||{}){__p.push(\'' +
				str.replace(/\\/g, '\\\\')
					.replace(/'/g, "\\'")
					.replace(c.interpolate, function(match, code) {
					return "'," + code.replace(/\\'/g, "'") + ",'";
					})
					.replace(c.evaluate || null, function(match, code) {
					return "');" + code.replace(/\\'/g, "'")
										.replace(/[\r\n\t]/g, ' ') + "; __p.push('";
					})
					.replace(/\r/g, '')
					.replace(/\n/g, '')
					.replace(/\t/g, '')
					+ "');}return __p.join('');";
			return tmpl;
			
			/** /
			var func = new Function('obj', tmpl);
			return data ? func(data) : func;
			/**/
		};

	define('tpl',[],function () {
		var tpl;

		var get, fs;
		if (typeof window !== "undefined" && window.navigator && window.document) {
			get = function (url, callback) {
				
				var xhr = tpl.createXhr();
				xhr.open('GET', url, true);
				xhr.onreadystatechange = function (evt) {
					//Do not explicitly handle errors, those should be
					//visible via console output in the browser.
					if (xhr.readyState === 4) {
						callback(xhr.responseText);
					}
				};
				xhr.send(null);
			};
		} else if (typeof process !== "undefined" &&
 				process.versions &&
 				!!process.versions.node) {
			//Using special require.nodeRequire, something added by r.js.
			fs = require.nodeRequire('fs');

			get = function (url, callback) {
				
				callback(fs.readFileSync(url, 'utf8'));
			};
		}
		return tpl = {
			version: '0.24.0',
			strip: function (content) {
				//Strips <?xml ...?> declarations so that external SVG and XML
				//documents can be added to a document without worry. Also, if the string
				//is an HTML document, only the part inside the body tag is returned.
				if (content) {
					content = content.replace(xmlRegExp, "");
					var matches = content.match(bodyRegExp);
					if (matches) {
						content = matches[1];
					}
				} else {
					content = "";
				}
				
				return content;
			},

			jsEscape: function (content) {
				return content.replace(/(['\\])/g, '\\$1')
					.replace(/[\f]/g, "\\f")
					.replace(/[\b]/g, "\\b")
					.replace(/[\n]/g, "")
					.replace(/[\t]/g, "")
					.replace(/[\r]/g, "");
			},

			createXhr: function () {
				//Would love to dump the ActiveX crap in here. Need IE 6 to die first.
				var xhr, i, progId;
				if (typeof XMLHttpRequest !== "undefined") {
					return new XMLHttpRequest();
				} else {
					for (i = 0; i < 3; i++) {
						progId = progIds[i];
						try {
							xhr = new ActiveXObject(progId);
						} catch (e) {}

						if (xhr) {
							progIds = [progId];  // so faster next time
							break;
						}
					}
				}

				if (!xhr) {
					throw new Error("require.getXhr(): XMLHttpRequest not available");
				}

				return xhr;
			},

			get: get,

			load: function (name, req, onLoad, config) {
				
				//Name has format: some.module.filext!strip
				//The strip part is optional.
				//if strip is present, then that means only get the string contents
				//inside a body tag in an HTML string. For XML/SVG content it means
				//removing the <?xml ...?> declarations so the content can be inserted
				//into the current doc without problems.

				var strip = false, url, index = name.indexOf("."),
					modName = name.substring(0, index),
					ext = name.substring(index + 1, name.length);

				index = ext.indexOf("!");
				
				if (index !== -1) {
					//Pull off the strip arg.
					strip = ext.substring(index + 1, ext.length);
					strip = strip === "strip";
					ext = ext.substring(0, index);
				}

				//Load the tpl.
				url = 'nameToUrl' in req ? req.nameToUrl(modName, "." + ext) : req.toUrl(modName + "." + ext);
				
				tpl.get(url, function (content) {
					content = template(content);
					
					if(!config.isBuild) {
					//if(typeof window !== "undefined" && window.navigator && window.document) {
						content = new Function('obj', content);
					}
					content = strip ? tpl.strip(content) : content;
					
					if (config.isBuild && config.inlineText) {
						buildMap[name] = content;
					}
					onLoad(content);
				});

			},

			write: function (pluginName, moduleName, write) {
				if (moduleName in buildMap) {
					var content = tpl.jsEscape(buildMap[moduleName]);
					write("define('" + pluginName + "!" + moduleName  +
  						"', function() {return function(obj) { " +
  							content.replace(/(\\')/g, "'").replace(/(\\\\)/g, "\\")+
  						"}});\n");
				}
			}
		};
		return function() {};	
	});
//>>excludeEnd('excludeTpl')
}());


define('tpl!templates/field.tpl', function() {return function(obj) { var __p=[],print=function(){__p.push.apply(__p,arguments);};with(obj||{}){__p.push('<table class="table table-bordered">    <thead>        <tr>            <th>#</th>            <th>Name</th>            <th>Animal Type</th>            <th>Sex</th>            <th>Parents</th>        </tr>    </thead>    <tbody>        '); for(var i = 0; i < animals.length; i++) { ; __p.push('            ', animals[i].render() ,'        '); } ; __p.push('    </tbody></table>');}return __p.join('');}});


define('tpl!templates/animal.tpl', function() {return function(obj) { var __p=[],print=function(){__p.push.apply(__p,arguments);};with(obj||{}){__p.push('<tr>    <td>', id ,'</td>    <td ', branded ? 'style="background-color: green; color: white"' : '' ,'>', name ,'</td>    <td>', type ,'</td>    <td>', gender ,'</td>    <td>', parents ,'</td></tr>');}return __p.join('');}});

define('animals/Animal',['tpl!../templates/animal.tpl'], function (template) {
  var id = 0;

  var Animal = {
    init: function (options) {
      this.id = options.id;
      this.type = options.type;
      this.branded = options.branded || false;
      this.parents = options.parents || [];
      this.sex = options.sex || 'male';
      this.name = options.name;
    },
    brand: function () {
      this.branded = true;
    },
    render: function () {
      return template({
        'id': this.id,
        'type': this.type,
        'name': this.name,
        'gender': this.sex,
        'branded': this.branded,
        'parents': this.parents.join(' + ')
      });
    }
  }

  // function Animal (opts) {
  //   if (!opts) opts = {}
  //   this.type = opts.type;
  //   this.parents = opts.parents || [];
  //   this.branded = opts.branded || false;
  //   this.sex = opts.sex || ['male','female'].random();
  //   this.name = opts.name;
  //
  //   function randomGender () {
  //     var genders = ['male','female'].random();
  //   }
  // }
  //
  // Animal.prototype.brand = function () {
  //   this.branded = true;
  //   return this;
  // }
  //
  // Animal.prototype.render = function () {
    // return template({
    //     'id': this.id,
    //     'type': this.type,
    //     'name': this.name,
    //     'gender': this.sex,
    //     'branded': this.branded,
    //     'parents': this.parents.join(' + ')
    // });
  // }

  return Animal;
});

define("Field", ['require','tpl!./templates/field.tpl','./animals/Animal'],function (Field) {
  var template = require('tpl!./templates/field.tpl');
  var Animal = require('./animals/Animal');

  var hasOwn = Object.prototype.hasOwnProperty;
  function has (obj, key) { return hasOwn.call(obj, key) }

  function Field (o) {
    var _animals = [];
    var _id = 0;
    var _maxAnimals = 100;

    /**
     * Add an animal to the field
     *
     * @param {animals}
     * @param {number} n - number of animals to add
     */
    function add (attributes) {
      if (_animals.length >= _maxAnimals) return false;

      var animal =  Object.create(Animal);
      attributes.id = ++_id

      animal.init(attributes);
      _animals.push(animal)

      console.log(animal)

      return _animals.map(function (x) {
        return x.id;
      });
    }

    /**
     * Attempt to find a love match for one lucky contestant.
     *
     * @param {object} all - the animals to look for a match
     * @param {object} subject - someone looking for love
     * @param {array} matches - animals that meet our requirements
     * @param {number} total - the max number of matches to return
     * @return {array}
     */
    function findLove (all, subject, matches, total) {
      var i, match, result = true;

      if (all.length === 0) {
        return matches;
      }

      i = all.randomIndex();
      match = all.splice(i, 1)[0];


      if (subject.sex === match.sex || subject.type !== match.type) {
        result = false
      } else {

        // prevent breeding with parents
        if (has(subject, 'parents')) {
          if (subject.parents.indexOf(match.id) !== -1) {
            result = false
          }
        }

        // prevent breeding with children
        if (has(match, 'parents')) {
          if (match.parents.indexOf(subject.id) !== -1) {
            result = false;
          }
        }

        // prevent breeding with siblings that have the same mother
        if (has(subject,'parents') && has(match,'parents')) {
          result = subject.parents[1] !== match.parents[1];
        }
      }

      if (result) {
        matches.push(match.id);
      }

      findLove(all, subject, matches);
    }


    function find (params) {
      var keys = Object.keys(params);
      return _animals.filter(function (animal) {
        var result = keys.filter(function (x) {
          return has(animal, x) && animal[x] === params[x];
        })

        return result.length === keys.length;
      })
    }

    function findMatchingPair () {
      var unbranded = find({ branded: false });
      var matches = [];

      console.log(unbranded[0])
      // findLove(unbranded, unbranded[0], matches, 1)

    }

    /**
     * Encourage two random animals to breed
     * @param {animalType}
     * @return {object|boolean}
     */
    function breed () {
      var unbranded = find({ branded: false });
      var matches = [];

      unbranded.find(function (x) {
        console.log(arguments)
      }, matches)

      // // get all unbranded animals
      // var random = unbranded.splice(Math.floor(Math.random() * unbranded.length),1);
      // var matches = [];
      //
      // findLove(unbranded, random, matches)
      //
      // console.log(matches);



      // recurse through the undbranded animals and try to find a match





      // if (!animalType) {
      //   throw new Error('missing animal type');
      // }

      // We only want animals of the provided animal type.
      // res = _animals.filter(function (x) {
      //   return x.type === animalType;
      // }).filter(isNotBranded);

      // if (res.length === 0) {
      //   throw new Error('there are no unbranded sheep');
      // }

      // filter parents, children & siblings
      // res = res.filter(filterParents)
      //          .filter(filterChildren)
      //          .filter(filterSiblings);

      // if (res.length === 0) {
      //   throw new Error('theres nobody to mate with');
      // }

      // group results by gender
      // res = res.reduce(reduceByGender, {})

      // we need both males and females to mate.
      // if (!res.hasOwnProperty(_M) || !res.hasOwnProperty(_F)) {
      //   throw new Error('we need both males and females to mate');
      // }

      // randomly select a lucky lady.
      // female = randArrayVal(res[_F]);


      // randomly select one of her matches.
      // male  = getAnimalById(randArrayVal(female.matches));

      // 50% chance of having a baby - fingers crossed!
      // if (!magicHappens()) return;

      // var child = _factory.createAnimal({
      //   type: animalType,
      //   parents: [male.id, female.id]
      // });
      //
      // female.children.push(child.id);
      // male.children.push(child.id);
      //
      // addAnimal(child);

      return child;
    }

    /**
     * Ren ders the Field
     * @return {string}
     */
    function render () {
      return template({'animals': _animals });
    }

    // Public Interface
    return {
      add: add,
      find: find,
      findLove: findLove,
      findMatchingPair: findMatchingPair,
      breed: breed
      // getAll: getAll
      // breed: breed,
      // brand: brand,
      // countAnimals: countAnimals,
      // isNotBranded: isNotBranded,

      // get: get,
      // getAll: getAll,
      // render: render
    }
  }

  return Field;

});

define('indexcontroller',['Field'], function(Field) {
    var createButton = document.getElementById('create-animal');
    var animalType = document.getElementById('animal-type');
    var animalGender = document.getElementById('animal-gender');
    var nameInput = document.getElementById('animalName');
    var fieldNode = document.getElementById('fieldContainer');
    var brandButton = document.getElementById('brand');
    var breedButton = document.getElementById('breed');
    var field = new Field();

    createButton.addEventListener('click', function (e) {
      field.addAnimal({
        name: animalName.value,
        gender:  animalGender.value,
        type: animalType.value
      })

      fieldNode.innerHTML = field.render();
    })

    // create a field, passing the node to render html t
    breedButton.addEventListener('click', function () {
      try {
        var child = field.breed(animalType.value);

        if(!child) {
          alert("mating attempt failed - better luck next time!")
        } else {
          alert("Success! we have a new baby "+ child.type +" called "+ child.name);
        }

        fieldNode.innerHTML = field.render();

      } catch (e) {
        alert(e.message);
      }
    })

    brandButton.addEventListener('click', function () {
      try {
        var animal = field.brandRandom();
        console.log(animal.name +' has been branded');
        fieldNode.innerHTML = field.render();
      } catch (e) {
        console.log('caught');
        alert(e.message);
      }
    });

    // render the field html
    fieldNode.innerHTML = field.render();
});

requirejs.config({
    paths: {
        tpl: 'lib/tpl/tpl',

    }
});
requirejs(['indexcontroller'], function(indexcontroller) {
    // extend array prototype
    Array.prototype.random = function () {
      return this[Math.floor(Math.random() * this.length)];
    }

    Array.prototype.randomIndex = function () {
      return Math.floor(Math.random() * this.length);
    }

});

define("bootstrap", function(){});

