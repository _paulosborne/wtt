define(['Field'], function(Field) {
    var createButton = document.getElementById('create-animal');
    var animalType = document.getElementById('animal-type');
    var animalGender = document.getElementById('animal-gender');
    var nameInput = document.getElementById('animalName');
    var fieldNode = document.getElementById('fieldContainer');
    var brandButton = document.getElementById('brand');
    var breedButton = document.getElementById('breed');
    var field = new Field();

    createButton.addEventListener('click', function (e) {
      field.addAnimal({
        name: animalName.value,
        gender:  animalGender.value,
        type: animalType.value
      })

      fieldNode.innerHTML = field.render();
    })

    // create a field, passing the node to render html t
    breedButton.addEventListener('click', function () {
      try {
        var child = field.breed(animalType.value);

        if(!child) {
          alert("mating attempt failed - better luck next time!")
        } else {
          alert("Success! we have a new baby "+ child.type +" called "+ child.name);
        }

        fieldNode.innerHTML = field.render();

      } catch (e) {
        alert(e.message);
      }
    })

    brandButton.addEventListener('click', function () {
      try {
        var animal = field.brandRandom();
        console.log(animal.name +' has been branded');
        fieldNode.innerHTML = field.render();
      } catch (e) {
        console.log('caught');
        alert(e.message);
      }
    });

    // render the field html
    fieldNode.innerHTML = field.render();
});
