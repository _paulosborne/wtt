<tr>
    <td><%= id %></td>
    <td <%= branded ? 'style="background-color: green; color: white"' : '' %>><%= name %></td>
    <td><%= type %></td>
    <td><%= gender %></td>
    <td><%= parents %></td>
</tr>
