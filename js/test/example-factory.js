var AnimalFactory = require('./AnimalFactory');
var animalFactory = new AnimalFactory();

console.log(animalFactory);

var sheep = animalFactory.createAnimal({
  type: 'sheep'
});
var cow = animalFactory.createAnimal({
  type: 'cow',
  name: 'Daisy'
});

var cow2 = animalFactory.createAnimal({
  type: 'cow',
  name: 'Daisy2'
});


var bird  = animalFactory.createAnimal({
  type: 'bird'
});

sheep.talk();
cow.talk();
cow2.talk();
bird.talk();
cow.fly();
bird.fly();

console.log('constructor', cow.constructor === bird.constructor);

console.log(JSON.stringify(sheep, null, 2));
console.log(JSON.stringify(cow, null, 2));
console.log(JSON.stringify(bird, null, 2));
