var Field = require('./Field');
var fs = require('fs');
var file = fs.readFileSync('./test/animals.mock.json');
var json = JSON.parse(file);

var field = new Field({
  animals: json
});

var result = field.breed('sheep');

console.log(JSON.stringify(result, null, 2));
