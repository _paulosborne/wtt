define(function (require) {
  var Field = require('Field');

  xdescribe('add [fn]', function () {
    var field;

    beforeEach(function () {
      field = new Field();
    })

    it('should add an animal to the field', function () {
      var ret = field.add({ type: 'sheep', sex: 'male' })
      expect(ret).toContain(1);
    })
  })

  describe('findMate [fn]', function () {
    var field = new Field();
    var func = field.findLove;
    var matches = [];
    var subject;

    beforeEach(function () {
      matches = [];
      subject = { id: 999, type: 'sheep', sex: 'male' };
    })

    it('should not match animals of the same sex', function () {
      var collection = [
        { id: 1, type: 'sheep', sex: 'female' },
        { id: 2, type: 'sheep', sex: 'male' }
      ]

      func(collection, subject, matches)
      expect(matches).toContain(1);
    })

    it('should not match animals of different types', function () {
      var collection = [
        { id: 69, type: 'sheep', sex: 'female' },
        { id: 3, type: 'cow', sex: 'female' }
      ]

      func(collection, subject, matches)
      expect(matches).toContain(69);
    })

    it('should not match parents', function () {
      var collection = [
        { id: 1, type: 'sheep', sex: 'male' },
        { id: 2, type: 'sheep', sex: 'female' },
        { id: 3, type: 'sheep', sex: 'female' }
      ]

      subject.parents = [1,2];

      func(collection, subject, matches)
      expect(matches).toContain(3);
    })

    it('should not match children', function () {
      var collection = [
        { id: 2, type: 'sheep', sex: 'female' },
        { id: 3, type: 'sheep', sex: 'female', parents: [999,2] }
      ]

      func(collection, subject, matches)
      expect(matches).toContain(2);
    })

    it('should not match siblings with the same mother', function () {
      var collection = [
        { id: 1, type: 'sheep', sex: 'male' },
        { id: 2, type: 'sheep', sex: 'female' },
        { id: 3, type: 'sheep', sex: 'female' }, // match
        { id: 4, type: 'sheep', sex: 'female', parents: [1,2] },
        { id: 5, type: 'sheep', sex: 'female', parents: [1,3] } // match
      ]

      subject.parents = [1,2];

      func(collection, subject, matches)
      expect(matches).toContain(3,4);
    })
  });

  describe('find [fn]', function () {
    it('should find all sheep', function () {
      var field = new Field();

      field.add({ type: 'sheep', sex: 'male' });
      field.add({ type: 'sheep', sex: 'male' });
      field.add({ type: 'cow', sex: 'female'});
      field.add({ type: 'cow', sex: 'male', branded: true });

      expect(field.find({ type: 'sheep' })).toBeArrayOfSize(2);
      expect(field.find({ type: 'cow' })).toBeArrayOfSize(2);
      expect(field.find({ sex: 'female' })).toBeArrayOfSize(1);
      expect(field.find({ branded: false })).toBeArrayOfSize(3);
    })
  })

  describe('findMatchingPair [function]', function () {
    it('should return unbranded animals', function () {
      var field = new Field();

      field.add({ type: 'sheep', sex: 'male' });
      field.add({ type: 'sheep', sex: 'female' });
      field.add({ type: 'cow', sex: 'female'});
      field.add({ type: 'cow', sex: 'male', branded: true });

      var result = field.findMatchingPair();

      console.log(JSON.stringify(result,null,2));

      // expect(result.hasOwnProperty('unbranded')).toBeTruthy();
    })
  })

})
